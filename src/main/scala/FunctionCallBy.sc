object FunctionCallBy{
  def something() ={
    println("inside calling something")
    5 // last line in scala is returned from the method
  }

  def callByValue(f:Int) ={
    println("call by value")
    println("x1:" + f)
    println("x2:" + f)
  }

  callByValue(something())

  def callByName(f: => Int) ={
    println("call by name")
    println("x1:" + f)
    println("x2:" + f)
  }

  callByName(something())
}