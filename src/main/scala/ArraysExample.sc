import scala.Predef.genericArrayOps

object ArraysExample{
  var movies = new Array[String](5)
  movies(0) = "Harry Potter"
  movies(1) = "Lord Of The Rings"
  movies(2) = "The Matrix"
  movies(3) = "Batman"
  movies
  movies(3) = "Ocean"
  movies
  var furnitures = Array("chair", "table", "sofa", "bed")
  furnitures
  furnitures(3) = "master-bed"
  furnitures
  println(furnitures)
  movies.foreach(m => print(m + ", "))


}