object TupleExample{

  // tuples are not iterable
  // maximum 22 items in tuple is possible
  val tpl1 = (1, 2.0, "Shibli", true)
  val tpl2 = "name" -> "shibli"
  val second = tpl1._3 // index starts from 1
  val ts = tpl2._2
  def div10(n:Int):Tuple2[Int, Int] = (n/10, n%10)
  div10(37)

  var (tens, ones) = div10(43)
  println(tens)

  def div100(n:Int):Tuple3[Int, Int, Int] = (n/100, n%100/10, n%10)

  var (h,t,o) = div100(251)
  println(h + "" + t + "" + o)


}