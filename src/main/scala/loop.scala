/**
  * Created by github.com/shibli049 on 2/13/18.
  */
var i = 5
while(i>0){
  print(i + ",")
  i-=1
}
println
i

val fruit = List("banana", "mango", "pineapple", "apple")

for(f <- fruit)
  println(f)
println


for (f <- fruit if(f.endsWith("e")))
  println(f)

for(i <- 1 to 5;
    j <- 1 to 5){
  println(i*j + ",")
}
println

// for each even number in 1 to 15 square it
for( n <- 1 to 15; e = n%2; if(e == 1)) yield n*n*n

val list = List((1,1), (2,4), (3,9))

for((a,b) <- list) yield a+b

var a = 5
var b = 10;

a
b*10+7

for (x <- 1 to 4; y <- 0 until 3) yield (x,y)

import scala.io.StdIn.{readLine, readInt}
var choice = 0
var result = ""

do{
  println("choose 1 to 4")
  choice = readInt
  result = choice match {
    case 1 => "java"
    case 2 => "scala"
    case 3 => "python"
    case 4 => "exit"
    case _ => "invalid"

  }
  println(choice)
  println(result)
}while(choice != 4)

