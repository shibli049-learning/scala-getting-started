object SpecialFunctions{
  //lambda: declaring and calling in same line
  ((x:Int) => x+1)(4)
  val f = (x:Int) => x+x
  f(5)
  // f(5) is intenally calls f.apply(5)
  f.apply(6)
  // function that takes a function as parameter
  def someOperation(f:(Int, Int)=>Int): Unit ={
    println(f(4,6))
  }

  val multiplication = (x:Int, y:Int) => x*y
  println()
  someOperation(multiplication)

  //greeting function will return a lambda function
  def greeting() = (name:String) => {
    "Hello " + name
  }

  val g = greeting()
  g("Shibli")

  //closure is a function that's return value depends on a variable outside of the function
  val y = 10
  var multiplier = (x:Int)=> x*y
  multiplier(4)
  


}