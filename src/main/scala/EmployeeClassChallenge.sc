object EmployeeClassChallenge{
  case class Employee(val id:Int, val fName:String, val lName:String){
    override def toString: String = {
      "Emplyee id: " + id + "\n" +
      "Full Name: " + fName + " " + lName
    }
  }

  val emp1 = Employee(12345, "Tahmim", "Ahmed")
  val emp2 = Employee(12346, "Abrar", "Ahmed")
  val emp3 = Employee(12347, "Ahmed", "Shibli")

  val l = List(emp1, emp2, emp3)
  l.foreach(println)

  println(emp1.fName)
}