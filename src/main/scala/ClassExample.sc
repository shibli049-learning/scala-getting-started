object ClassExample{

  class Computer(val maker:String, val model:String, val ram:String){
    override def toString: String = {
      "[" + maker + ", " + model + ", " + ram + "]"
    }
  }

  val computers = List(
    new Computer("Dell", "Laptop", "16GB"),
    new Computer("Mac", "Mini", "8GB"),
    new Computer("HP", "Notebook", "4GB")
  )

  val first = computers.head
  val tal = computers.tail

//  val computers
}