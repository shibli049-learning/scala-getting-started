import java.io.FileNotFoundException

import io.Source._

object ErrorHandling{
  try {
    for (line <- fromFile("/home/tigerit/Untitled4.sql").getLines()) {
      println(line)
    }
    val v = 10/0
  }catch {
    case ex : FileNotFoundException => println("File not found!")
    case default => {
      println("other")
      default.printStackTrace()
    }
  }
}