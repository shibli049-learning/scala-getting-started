object OperatorAndHigherOrderFuntions{
  val double = (i:Int) => i * 2
  val triple = (i:Int) => i * 3

  def highOrder(i:Int, y:Int=>Int) = {
    y(i)
  }

  highOrder(10, triple)

  def sayHello() = (name:String) => {"Hello " + name}

  var msg = sayHello()

  msg("hello")

  //closure: a function whose return value depends on a variable outside the function
  var y = 10
  val multiplier = (x:Int) => x * y

  multiplier(7)

  //bitwise operator
  var x1 = 3 // 0011
  var x2 = 9 // 1001

  x1 & x2
  x1 | x2
  x1 ^ x2

  // cons `::`
  val numbers = 1::2::3::Nil

  var f = 5.251f
  var d = 5.251
  d == f


}