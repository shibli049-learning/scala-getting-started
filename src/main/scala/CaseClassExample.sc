object CaseClassExample{
  case class Money(val amount:Int = 1, val currency:String = "USD"){
    def + (other: Money):Money = Money(amount + other.amount, currency)

    def == (other: Money) : Boolean = {
      amount.equals(other.amount) &&
      currency.equals(other.currency)
    }
  }

  val defaultAmount = new Money()
  val fiveDollars = new Money(5)
  val euros = new Money(currency = "EUR")

  val tenEur = euros.copy(10)

  val fifteenEur = tenEur + fiveDollars

  println( defaultAmount == euros )
}