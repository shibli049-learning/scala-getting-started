/**
  * Created by github.com/shibli049 on 2/12/18.
  */
object Hello{
  def main(args: Array[String]) {
    val name:String = args(0)
    print("Hello " + name)
  }
}

import java.util.Date
import java.util.Locale
import java.text.DateFormat
import java.text.DateFormat._

//single line comment

/*
* comments in scala is same as java
* */

object PrintDate {
  def main(args: Array[String]): Unit = {
    val now = new Date
    val df = getDateInstance(LONG, Locale.UK)
    println(df.format(now))
  }
}
