object MyFirstWorksheet{
  println("welcome to my 1st scala worksheet.")
  2+3
  var x = 5
  var check = false
  var pi = 3.1416
  var fibs = List(1,1,2,3,5,8,13,21,34,55)
  var msg = "Make Scala great!"
  var letter = 'a'
  var range = 1 to 15
  def add(a:Int, b:Int) = a+b
  add(5,7)
  var r : AnyVal = 10
  r = 15
  r = 4.5

  val anon = (a:Double) => a+1
  anon(9)

}