object ListExample{
  //list ------------------
  val empty:List[Nothing] = List()
  var list:List[Any] = List(1, 2.0)
  var list2:List[Any] = List(1, 2.0)
  list ::=  "hello"
  list
  var rlist2 = "hello2444" :: 1 :: 2.0 :: Nil
  var nums = List.range(10,0,-2)
  println("l2: " + list2)
  // merge list using :::
  var l4 =  rlist2 ::: nums
  println("l4: " + l4)
  val l5 = List.concat(list, nums)
  println("end")

  var rainbow = List("v", "i", "b", "g", "y", "o", "r")
  for(c<-rainbow){
    println(c)
  }

  var c = ""
  // _ means current item
  rainbow.foreach(c+=_)
  println(c)

  var dNums = nums.map(_*2)

}