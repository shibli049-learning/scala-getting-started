object BasicTypesAndOperations{
  // Unit = Void

  //match expression
  val day1 = "FRI"
  val day2 = "SUN"
  val day3 = "TTT"

  def dayType(day:String) = {
    day match {
      case "SUN" | "MON" | "TUE" | "WED" | "THU" => "WEEKDAY"
      case "FRI" | "SAT" => "WEEKEND"
      case _ => "INVALID"
    }
  }

  dayType(day1)
  dayType(day2)
  dayType(day3)

  var grades = List(100, 99, 75, 67, 89, 85)

  var count:Double = grades.length

  val sum = grades.foldLeft(0.0)(_+_)

  def avg(c:Double, s:Double) = s/c

  println(avg(count,sum))

}