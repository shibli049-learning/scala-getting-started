/**
  * Created by github.com/shibli049 on 2/20/18.
  */

import io.StdIn._

object ReadFromConsole extends App {

  var done: Boolean = false;
  do {
    var name = readLine("name:")
    print("score: ")
    var score = readDouble()
    println("success!" + name + " score: " + score)
    var yn = readLine("ext: y/n?")
    done = yn.equalsIgnoreCase("y")
  } while (!done)


}
