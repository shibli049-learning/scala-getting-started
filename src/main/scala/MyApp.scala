/**
  * Created by github.com/shibli049 on 2/13/18.
  */
class MyApp extends App {

  def divisors(n:Int): List[Int] = {
    for( i <- List.range(1, n+1) if( n%i == 0) ) yield i
  }

  def isPrime(n:Int) = divisors(n).length == 2


  if(isPrime(7)) {
    println("prime")
  }else{
    println("non-prime")
  }
}





