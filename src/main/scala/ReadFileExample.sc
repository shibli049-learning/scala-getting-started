import io.Source._
object ReadFileExample{
  val fileName = "/home/tigerit/Untitled4.sql"
  for(line <- fromFile(fileName).getLines()){
    println(line)
  }

  val lines = fromFile(fileName).getLines().toList

  lines.foreach(line => println(">>" + line))

}