object SetsAndMapsExample{
  val fruits = Set("apple", "banana", "orange", "cherry", "apple")
  fruits("banana")
  fruits("peach")
  var series = Set(1,2,3,4,5,6,5,4,3,2,1)
  series += 7
  println(series)
  if(series.isEmpty){
    println("empty")
  }else{
    println(series.head)
    println(series.tail)
  }
  //combine two sets
  var comb = fruits ++ series
  println(comb)

  var comb2 = comb ++ fruits
  println(comb2.equals(comb))
  val s2 = Set(4,5,6,8)
  //common/intersection
  s2.&(series)
  series.&(s2)
  //union
  series.|(s2)
  //map #####################
  var map = Map("milk" -> 1, "bread" -> 1, "juice" -> 1, "eggs" -> 4)
  map("milk")
  map += ("salt" -> 2)
  map("eggs")
  map.get("salt")
  map.get("werwe")
  map.getOrElse("salt", None)
  map.getOrElse("werwe", None)

  var reversedMap = for((key, value) <- map) yield (value, key)

  println("map:")
  map.foreach(println)

  println("reversed map:")
  reversedMap.foreach(println)

}